import React, { Component } from "react";

import Birthday from "./Birthday";
import AgeStats from "./AgeStats";

class App extends Component {
  render() {
    return (
      <div>
        <AgeStats />
        <Birthday />
      </div>
    );
  }
}
export default App;
