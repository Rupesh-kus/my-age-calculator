import React, { Component } from "react";
import { connect } from "react-redux";
import partyPopper from "./party-popper.jpg";
import "../App.css";

class AgeStats extends Component {
  render() {
    let todayAge = this.props.date;
    return (
      <div className='display-container'>
        <h4> Congrats on {todayAge}!</h4>
        <img
          src={partyPopper}
          style={{ height: 50, width: 50 }}
          alt='party-popper'
          className='party-popper'
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("mapstatetoprops", state);
  return {
    date: state.date,
  };
};

export default connect(mapStateToProps, null)(AgeStats);
