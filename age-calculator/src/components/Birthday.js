import React, { Component } from "react";
import { connect } from "react-redux";
import { getBirthdayDate } from "../actions";

class Birthday extends Component {
  state = {
    date: "",
  };

  handleOnchange = (e) => {
    console.log(e.target.value);
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.getBirthdayDate(this.state.date);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            <h4>Enter Your Birthday</h4>
          </label>
          <br />
          <input
            type='date'
            className='form-control'
            onChange={this.handleOnchange}
            name='date'
            value={this.state.date}
          />
          <br />
          <button type='submit' className='btn btn-info'>
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default connect(null, { getBirthdayDate })(Birthday);
