import { combineReducers } from "redux";
import DateReducer from "./DateReducer";

const rootReducers = combineReducers({
  date: DateReducer,
});

export default rootReducers;
