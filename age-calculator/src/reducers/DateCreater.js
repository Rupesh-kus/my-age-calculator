export function createDate(date) {
  let today = new Date().getTime();
  let other_date = new Date(date).getTime();
  let difference = Math.abs(today - other_date);

  let days = Math.floor(difference / (1000 * 3600 * 24));
  let years = Math.floor(days / 365);
  days -= years * 365;
  let months = Math.floor(days / 31);
  days -= months * 31;

  let dd = `${years} years, ${months} months, and ${days} days`;
  console.log("create Date functin", dd);
  return `${years} years, ${months} months, and ${days} days`;

  // return date;
}
