import { GET_DATE } from "../actions";
import { createDate } from "./DateCreater";

const dates = (state = "", action) => {
  switch (action.type) {
    case GET_DATE:
      let date = createDate(action.payload);
      return date;
    default:
      return state;
  }
};

export default dates;
