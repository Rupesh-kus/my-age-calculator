export const GET_DATE = "GET_DATE";

export function getBirthdayDate(date) {
  const action = {
    type: GET_DATE,
    payload: date,
  };
  return action;
}
